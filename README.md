API juegotornos  
Ejercicio de José Pérez Moreno para el Python Challenge de MeetUp

INSTALACIÓN (Linux)

Necesario Python v 3.6 y pip

git clone https://joseprzmoreno@bitbucket.org/joseprzmoreno/juegotornos_jpm.git


Crear base de datos. En tu instalación mysql local:
create database jt838383_places;  

create database jt838383_characters;  

Editar archivo config.py con los parámetros de tu base de datos local

(Se recomienda usar un entorno virtual de Python)  
Por ejemplo:  
virtualenv envjt  
source envjt/bin/activate  

(Dependiendo de la configuración podría ser necesario utilizar "python3" y "pip3" en lugar de "python" y "pip")

Instalar dependencias:   
pip install -r requirements.txt  

Crear las entidades:  
python dbconfig.py db init --multidb  
python dbconfig.py db migrate  
python dbconfig.py db upgrade  
  
Poblar base de datos:  
python populate_db.py

Test unitarios (están pensados para los datos de prueba, con la API de lugares):  
python api_places.py  
python test_places.py  

Probar las 3 apis/servicios en Swagger:  
python api_places.py  
	--> http://localhost:8081  
python api_characters.py    
	--> http://localhost:8082  
python api_juegotornos.py  
	--> http://localhost:8083   
  






