import unittest
import requests
import json
from dbconfig import Place, Character
from api_places import *

class TestPlaces(unittest.TestCase):
    
    def test_01_list_all(self):
        r = requests.get('http://localhost:8081/place-microservice/v1/places')
        places = r.json()
        print(places)
        orig_names = ["Kwarz","Nidal","Dorm","Tureke"]
        place_names = []
        for place in places:
            place_names.append(place["name"])
        self.assertEqual(orig_names, place_names)

    def test_02_get_place(self):
        r = requests.get('http://localhost:8081/place-microservice/v1/places/2')
        place = r.json()
        print(place)
        self.assertEqual(place["name"], "Nidal")

    def test_03_get_nonexistent_place(self):
        r = requests.get('http://localhost:8081/place-microservice/v1/places/99')
        self.assertEqual(r.status_code,404)

    def test_04_create_place(self):
        url = "http://localhost:8081/place-microservice/v1/places"
        info = {"id":0,"name":"Ytrelel"}
        r = requests.post(url, json=info)
        self.assertEqual(json.loads(r.text)["name"],"Ytrelel")

    def test_05_edit_place(self):
        url = "http://localhost:8081/place-microservice/v1/places/3"
        info = {"id":0,"name":"Darm"}
        r = requests.put(url, json=info)
        self.assertEqual(json.loads(r.text)["name"], "Darm")

    def test_06_delete_place(self):
        requests.delete('http://localhost:8081/place-microservice/v1/places/4')
        r = requests.get('http://localhost:8081/place-microservice/v1/places')
        places = r.json()
        place_names = []
        for place in places:
            place_names.append(place["name"])
        self.assertNotIn("Tureke",place_names)

if __name__ == '__main__':
    unittest.main()





    
        
