from flask import Flask
from flask_restplus import Api, Resource, fields, marshal
from werkzeug.contrib.fixers import ProxyFix
from dbconfig import Place, Character, app, db
#from api_places import PlaceDAO, PlaceResource
from api_characters import CharacterDAO

jt_app = Flask(__name__)
jt_app.wsgi_app = ProxyFix(jt_app.wsgi_app)
api = Api(jt_app, version='1.0', title='juegotornos_api',
    description='An API to manage places and characters',
)

jt_ns = api.namespace('juegotornos', description='juegotornos operations')

character = api.model('Character', {
    'id': fields.Integer(readOnly=True, description='The character unique identifier'),
    'name': fields.String(required=True),
    'place_id':fields.Integer(),
    'is_alive':fields.Boolean(),
    'is_the_monarch':fields.Boolean()
    })

jt_place = api.model('Place', {
    'id': fields.Integer(readOnly=True, description='The place unique identifier'),
    'name': fields.String(required=True, description='The name of the place'),
    'characters': fields.List(fields.Nested(character))
})
db.init_app(jt_app)

class jtPlaceDAO(object):
    
    def __init__(self):
        self.counter = 0
        self.places = Place.query.all()
        for place in self.places:
            place.characters = []
            characterDAO_o = CharacterDAO()
            for character in characterDAO_o.characters:
                if character.place_id == place.id:
                    if place.monarch == character.id:
                        character.is_the_monarch = True
                    else:
                        character.is_the_monarch = False
                    place.characters.append(character)
                
    def get(self, id):
        for place in self.places:
            if place.id == id:
                return place
        pl_api.abort(404, "Place {} doesn't exist".format(id))

jtPlaceDAO = jtPlaceDAO()

@jt_ns.route('/v1/places')
class jtPlaceList(Resource):
    '''Shows a list of all places and their inhabitants, and lets you POST to add new places'''
    @jt_ns.doc('list_places')
    @jt_ns.marshal_list_with(jt_place)
    def get(self):
        '''List all places'''
        return jtPlaceDAO.places

@jt_ns.route('/v1/places/<int:id>')
@jt_ns.response(404, 'Place not found')
@jt_ns.param('id', 'The place identifier')
class jtPlaceResource(Resource):
    '''Show a single place item and lets you delete them'''
    @jt_ns.doc('get_place')
    @jt_ns.marshal_with(jt_place)
    def get(self, id):
        '''Fetch a given resource'''
        return jtPlaceDAO.get(id)

if __name__ == '__main__':
    jt_app.run(port=8083,debug=True)


