from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import config

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@localhost:{}/places'.format(config.db_user,config.db_password,config.db_port)
DBCHAR_URI = 'mysql+pymysql://{}:{}@localhost:{}/characters'.format(config.db_user,config.db_password,config.db_port)

SQLALCHEMY_BINDS = {
    #'dbplc': SQLALCHEMY_DATABASE_URI,
    'dbchar': DBCHAR_URI
}

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_BINDS'] = SQLALCHEMY_BINDS
db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)

class Place(db.Model):

    __tablename__ = 'places'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    monarch = db.Column(db.Integer)

    def __init__(self,name):
        self.name = name
        db.session.add(self)
        db.session.commit()

    def set_monarch(self,character_id):
        self.monarch = character_id
        db.session.commit()
        return self

    def set_name(self,name):
        self.name = name
        db.session.commit()
        return self

    def remove(self):
        #deberia borrar los personaje de este lugar       
        db.session.delete(self)
        db.session.commit()
        return True

class Character(db.Model):

    __tablename__ = 'characters'
    __bind_key__ = 'dbchar'

    id = db.Column(db.Integer, primary_key=True)
    place_id = db.Column(db.Integer)
    name = db.Column(db.String(255), unique=True, nullable=False)
    is_alive = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self,name,place_id,is_alive):
        self.name = name
        self.place_id = place_id
        self.isAlive = is_alive
        db.session.add(self)
        db.session.commit()

    def set_name(self,name):
        self.name = name
        db.session.commit()
        return self

    def set_is_alive(self,is_alive):
        self.is_alive = is_alive
        db.session.commit()
        return self

    def set_place_id(self,place_id):
        self.place_id = place_id
        db.session.commit()
        return self
    
if __name__ == '__main__':
    manager.run()
