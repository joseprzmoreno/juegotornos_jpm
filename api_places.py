from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from dbconfig import Place, Character, app, db

#app = Flask(__name__)

app.wsgi_app = ProxyFix(app.wsgi_app)
pl_api = Api(app, version='1.0', title='place-microservice',
    description='An API to manage places',
)

pl_ns = pl_api.namespace('place-microservice', description='places operations')

place = pl_api.model('Place', {
    'id': fields.Integer(readOnly=True, description='The place unique identifier'),
    'name': fields.String(required=True, description='The name of the place')
})


class PlaceDAO(object):
    def __init__(self):
        self.counter = 0
        self.places = Place.query.all()

    def get(self, id):
        for place in self.places:
            if place.id == id:
                return place
        pl_api.abort(404, "Place {} doesn't exist".format(id))

    def create(self, data):
        place = Place(data["name"])
        return place

    def update(self, id, data):
        place = self.get(id)
        place.set_name(data["name"])
        return place

    def delete(self, id):
        #en version mas sofisticada, borraria los personajes del lugar
        place = self.get(id)
        self.places.remove(place)

PlaceDAO_o = PlaceDAO()

@pl_ns.route('/v1/places')
class PlaceList(Resource):
    '''Shows a list of all places, and lets you POST to add new places'''
    @pl_ns.doc('list_places')
    @pl_ns.marshal_list_with(place)
    def get(self):
        '''List all places'''
        return PlaceDAO_o.places

    @pl_ns.doc('create_place')
    @pl_ns.expect(place)
    @pl_ns.marshal_with(place, code=201)
    def post(self):
        '''Create a new place. Id value will be ignored'''
        return PlaceDAO_o.create(pl_api.payload), 201

@pl_ns.route('/v1/places/<int:id>')
@pl_ns.response(404, 'Place not found')
@pl_ns.param('id', 'The place identifier')
class PlaceResource(Resource):
    '''Show a single place item and lets you delete them'''
    @pl_ns.doc('get_place')
    @pl_ns.marshal_with(place)
    def get(self, id):
        '''Fetch a given resource'''
        return PlaceDAO_o.get(id)

    @pl_ns.doc('delete_place')
    @pl_ns.response(204, 'Place deleted')
    def delete(self, id):
        '''Delete a place given its identifier'''
        PlaceDAO_o.delete(id)
        return '', 204

    @pl_ns.expect(place)
    @pl_ns.marshal_with(place)
    def put(self, id):
        '''Update a place given its identifier. ID value inside model will be ignored'''
        return PlaceDAO_o.update(id, pl_api.payload)

if __name__ == '__main__':
    app.run(port=8081,debug=True)
