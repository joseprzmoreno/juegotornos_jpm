from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix
from dbconfig import Place, Character, app, db
#from api_places import PaceDAO, PlaceResource

#app = Flask(__name__)

app.wsgi_app = ProxyFix(app.wsgi_app)
char_api = Api(app, version='1.0', title='character-microservice',
    description='An API to manage Juegotornos characters',
)

char_ns = char_api.namespace('character-microservice', description='character operations')

place = char_api.model('Place', {
    'id': fields.Integer(readOnly=True, description='The place unique identifier'),
    'name': fields.String(required=True, description='The name of the place')
})

character = char_api.model('Character', {
    'id': fields.Integer(readOnly=True, description='The character unique identifier'),
    'name': fields.String(required=True),
    'place_id':fields.Integer(),
    'is_alive':fields.Boolean(default=False)
    })
db.init_app(app)

class CharacterDAO(object):
    def __init__(self):
        self.counter = 0
        self.characters = Character.query.all()

    def get(self, id):
        for character in self.characters:
            if character.id == id:
                return character
        char_api.abort(404, "Character {} doesn't exist".format(id))

    def create(self, data):
         #Si esta vivo no debe dejar crear sin lugar (mas sofist., comprobar que el lugar exista)
        if data["is_alive"] and (data["place_id"]<=0 or data["place_id"]==None):
            char_api.aport(404, "Living characters need a place id")
        character = Character(data["name"],data["place_id"],
                              data["is_alive"])
        return character

    def update(self, id, data):
        #Si esta vivo no debe dejar editar sin lugar (mas sofist., comprobar que el lugar exista)
        if data["is_alive"] and (data["place_id"]<=0 or data["place_id"]==None):
            char_api.aport(404, "Living characters need a place id")
        character = self.get(id)
        character.set_is_alive(data["is_alive"])
        character.set_name(data["name"])
        character.set_place_id(data["place_id"])        
        return character

    def delete(self, id):
        #en version mas sofisticada, lo borraria en la tabla places si es rey
        character = self.get(id)
        self.characters.remove(character)

CharacterDAO_o = CharacterDAO()

@char_ns.route('/v1/characters')
class CharacterList(Resource):
    '''Shows a list of all characters, and lets you POST to add new character'''
    @char_ns.doc('list_characters')
    @char_ns.marshal_list_with(character)
    def get(self):
        '''List all characters'''
        return CharacterDAO_o.characters

    @char_ns.doc('create_character')
    @char_ns.expect(character)
    @char_ns.marshal_with(character, code=201)
    def post(self):
        '''Create a new character. Id value inside model will be ignored.'''
        return CharacterDAO_o.create(char_api.payload), 201

@char_ns.route('/v1/characters/<int:id>')
@char_ns.response(404, 'Character not found')
@char_ns.param('id', 'The character identifier')
class CharacterResource(Resource):
    '''Show a single character item and lets you delete them'''
    @char_ns.doc('get_character')
    @char_ns.marshal_with(character)
    def get(self, id):
        '''Fetch a given resource'''
        return CharacterDAO_o.get(id)

    @char_ns.doc('delete_character')
    @char_ns.response(204, 'Character deleted')
    def delete(self, id):
        '''Delete a character given its identifier'''
        CharacterDAO_o.delete(id)
        return '', 204

    @char_ns.expect(character)
    @char_ns.marshal_with(character)
    def put(self, id):
        '''Update a character given its identifier. Id value inside model will be ignored.'''
        return CharacterDAO_o.update(id, char_api.payload)

if __name__ == '__main__':
    app.run(port=8082,debug=True)
